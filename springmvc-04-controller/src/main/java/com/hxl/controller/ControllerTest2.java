package com.hxl.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

//代表这个类会被spring接管，这个注解的类种的所有方法，如果返回值是String并且有具体的页面可以跳转，那么就会被视图解析器解析
@Controller
public class ControllerTest2 {
    @RequestMapping("/t2")
    public String test(Model model){
        model.addAttribute("msg", "ControllerTest2");
        return "test";
    }
}
