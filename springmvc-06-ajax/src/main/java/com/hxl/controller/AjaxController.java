package com.hxl.controller;

import com.hxl.pojo.User;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

//自动返回一个字符串
@RestController
public class AjaxController {

    @RequestMapping("/t1")
    public String test(){
        return "hello";
    }

    @RequestMapping("/a1")
    public void ajax1(String name , HttpServletResponse response) throws IOException {
        if ("wmm".equals(name)){
            response.getWriter().print("true");
        }else{
            response.getWriter().print("false");
        }
    }

    @RequestMapping("/a2")
    public List<User> a2(){
        ArrayList<User> list = new ArrayList<User>();
        list.add(new User("王木木",1,"男"));
        list.add(new User("hxl",2,"男"));
        return list;
    }

    @RequestMapping("/a3")
    public String a3(String name, String pwd){
        String msg = null;
        if(name != null){
            if("admin".equals(name)){
                msg = "ok";
            }else{
                msg="用户名有误";
            }
        }
        if(pwd != null){
            if("123456".equals(pwd)){
                msg = "ok";
            }else{
                msg="密码有误";
            }
        }
        return msg;
    }
}
