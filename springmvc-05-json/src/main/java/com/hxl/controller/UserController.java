package com.hxl.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.hxl.pojo.User;
import com.hxl.util.JsonUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

@Controller
public class UserController {
    //@RequestMapping(value="/j1",produces = "application/json;charset=utf-8")
    //统一配置json乱码后
    @RequestMapping("/j1")
    @ResponseBody //他不会去走视图解析器，会直接返回一个字符串
    private String jso1() throws JsonProcessingException {
        //jackson,ObjectMapper
        ObjectMapper mapper = new ObjectMapper();
        //创建一个对象
        User user = new User("王木木", 18, "男");

        String str = mapper.writeValueAsString(user);
        return str;
    }

    @RequestMapping("/j2")
    @ResponseBody
    private String jso2() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();

        ArrayList<User> userList = new ArrayList<User>();
        User user1 = new User("王木木1", 18, "男");
        User user2 = new User("王木木2", 18, "男");
        User user3 = new User("王木木3", 18, "男");
        User user4 = new User("王木木4", 18, "男");

        userList.add(user1);
        userList.add(user2);
        userList.add(user3);
        userList.add(user4);

        String str = mapper.writeValueAsString(userList);
        return str;
    }

    @RequestMapping("/j3")
    @ResponseBody
    private String jso3() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        Date date = new Date();
        //自定义日期的格式
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
        simpleDateFormat.format(date);
        //ObjectMapper，时间解析后的默认格式为Timestamp，时间戳
        return mapper.writeValueAsString(simpleDateFormat);
    }
    @RequestMapping("/j4")
    @ResponseBody
    private String json4() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        //不适用时间戳的方式
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS,false);
        //自定义日期的格式
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
        mapper.setDateFormat(simpleDateFormat);
        Date date = new Date();
        //ObjectMapper，时间解析后的默认格式为Timestamp，时间戳
        return mapper.writeValueAsString(date);
    }
    @RequestMapping("/j5")
    @ResponseBody
    private String json5() throws JsonProcessingException {
        Date date = new Date();
        return JsonUtils.getJson(date,"yyyy-MM-dd HH-mm-ss");
    }

    @RequestMapping("/j6")
    @ResponseBody
    private String json6(){
        ArrayList<User> userList = new ArrayList<User>();
        User user1 = new User("王木木1", 18, "男");
        User user2 = new User("王木木2", 18, "男");
        User user3 = new User("王木木3", 18, "男");
        User user4 = new User("王木木4", 18, "男");

        userList.add(user1);
        userList.add(user2);
        userList.add(user3);
        userList.add(user4);
        //java对象转json字符串
        String str = JSON.toJSONString(userList);
        String str2 = JSON.toJSONString(user1);
        System.out.println(str);
        //json字符串转java对象
        User u1 = JSON.parseObject(str2,User.class);
        System.out.println(u1);
        //java对象转json字对象
        JSONObject jo1 = (JSONObject)JSON.toJSON(user2);
        System.out.println(jo1);
        //json对象转java对象
        User to_java_user = JSON.toJavaObject(jo1,User.class);
        System.out.println(to_java_user);
        return "test";
    }
}
